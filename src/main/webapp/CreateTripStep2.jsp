<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cr�ation de D�placement</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
 <link rel="stylesheet" href="../Style/css" type="text/css">
</head>
<body style="background-color:#F3F1F1">

<div class= "container" style="padding:50px;">

	<h1 style="display:flex;justify-content:center;margin-bottom:30px;font-weight:bold;">S�lectionner l'infirmi�re : </h1>
	<table class="table">
		<tr>
			<th scope="row"> nom </th>
			<th scope="row"> prenom </th>
			<th scope="row"> </th>
		</tr>
		<c:forEach items="${ infirmieres }" var="infirmiere">
		<tr>
			<td> ${ infirmiere.nom } </td>
			<td> ${ infirmiere.prenom } </td>
			<td> <a href="addTrip3?patient_id=${ patient_id }&infirmiere_id=${ infirmiere.id }" class="btn btn-danger btn-sm"> Ajouter cette infirmi�re </a> </td>
		
		</tr>
		</c:forEach>
	</table>
</div>
<div class="span12" style="text-align:center">
	<a href="/demo">
      <button class="btn btn-large btn-primary">Retour � l'accueil</button>
    </a>
</div>
<br><br><br>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>