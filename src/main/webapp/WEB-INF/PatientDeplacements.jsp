<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link rel="stylesheet" type="text/css" href="WEB-INF/style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


<title>Deplacements li� au patient </title>
</head>
<body style="background-color:#F3F1F1">
<div class= "container" style="padding:50px;">

<h1 style="display:flex;justify-content:center;margin-bottom:30px;font-weight:bold;">D�placements associ�s au patient : </h1>
	<table class="table">
		<tr>
			<th scope="row"> Identifiant </th>
			<th scope="row"> Identifiant du patient </th>
			<th scope="row"> Identifiant de l'infirmi�re </th>
			<th scope="row"> Date du d�placement </th>
			<th scope="row"> Co�t du d�placement </th>
			<th scope="row"> </th>
		</tr>
		<c:forEach items="${ deplacements }" var="deplacement">
		<tr>
			<td> ${ deplacement.id } </td>
			<td> ${ deplacement.patient_id } </td>
			<td> ${ deplacement.infirmiere_id } </td>
			<td> ${ deplacement.date } </td>
			<td> ${ deplacement.cout } </td>		
			<td> <a href="updateDeplacementPatient?id=${ deplacement.id }" class="btn btn-warning"> Modifier le d�placement </a> </td>
			<td> <a href="deletePatientDeplacement?id=${ deplacement.id }&patient_id=${ deplacement.patient_id }" class="btn btn-danger btn-sm"> Supprimer le d�placement </a> </td>
		</tr>
		
		</c:forEach>
	</table>
	
</div>
<div class="span12" style="text-align:center">
	<a href="/demo">
      <button class="btn btn-large btn-primary">Retour � l'accueil</button>
    </a>
</div>
<br><br><br>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>