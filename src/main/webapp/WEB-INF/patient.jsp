<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


<title>Mes patients</title>
</head>
<body style="background-color:#F3F1F1">

<div class= "container" style="padding:50px;">
<h4 style="display:flex;justify-content:center;margin-bottom:30px;font-weight:bold;">Mes patients</h4>
<table class="table">
<tr>
<th><label for="name">Nom: </label></th>
<th><label for="prenom">Prenom: </label></th>
<th><label for="sexe">Sexe: </label></th>
<th><label for="dateDeNaissance">Date de naissance: </label></th>
<th><label for="numeroSecuriteSocial">Num securite social: </label></th>
<th></th>
<th></th>
</tr>
<c:forEach items="${patients}" var="patient">
<tr>
<td>
<input class="form-control form-control-sm" type="text" name="name" id="name" value="${patient.nom}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="prenom" id="prenom" value="${patient.prenom}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="sexe" id="sexe" value="${patient.sexe}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="dateDeNaissance" id="dateDeNaissance" value="${patient.dateDeNaissance}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="numeroSecuriteSocial" id="numeroSecuriteSocial" value="${patient.numeroSecuriteSocial}"></td>
<td> <a href="deplacementpatient?id=${ patient.id }" class="btn btn-warning"> d�placements </a></td>
<td> <a href="patientUpdateController?id=${ patient.id }" class="btn btn-warning"> MAJ </a></td>
<td><a href="patient?id=${ patient.id }" class="btn btn-danger"> X </a></td>
</tr>
</c:forEach>
</table>
<div style="display:flex;justify-content:space-around;">
<a href="#" class="btn btn-primary" onClick="AfficherMasquer()">Ajouter un patient</a>
<a href="/demo">
      <button class="btn btn-large btn-secondary">Retour � l'accueil</button>
</a>
</div>
<div style="display:none;" id="formAjout">
<form class="table table-dark" method="post" action="patient">
<table class="table table-stripped">
<tr>
<th><label for="adresse">Informations :</label></th>
</tr>
<tr>
<td><input class="form-control form-control-sm" type="text" name="nomAdd" id="nomAdd" placeholder="Nom" required></td>
<td><input class="form-control form-control-sm" type="text" name="prenomAdd" id="prenomAdd" placeholder="Prenom" required></td>
<td>
<select class="custom-select" name="sexeAdd" id="sexeAdd" required>
<option selected value="">-Choisissez le sexe-</option>
<option value="homme">Homme</option>
<option value="femme">Femme</option>
</select>
</td>
<td><input class="form-control form-control-sm" type="text" name="dateDeNaissanceAdd" id="dateDeNaissanceAdd" placeholder="Date de naissance" required></td>
<td><input class="form-control form-control-sm" type="text" name="numeroSecuriteSocialAdd" id="numeroSecuriteSocialAdd" placeholder="Numero securite social" required></td>
<td>
<select class="custom-select" name="infirmiere_id" id="infirmiere_id" required>
<option selected value="">--Nom infirmi�re--</option>
<c:forEach items="${nurses}" var="nurse">
<option  value="${nurse.key}">${nurse.value}</option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<th><label for="adresse">Adresse :</label></th>
</tr>
<tr>
<td><input class="form-control form-control-sm" type="text" name="numero" id="numero" placeholder="num�ro" required></td>
<td><input class="form-control form-control-sm" type="text" name="rue" id="rue" placeholder="rue" required></td>
<td><input class="form-control form-control-sm" type="text" name="cp" id="cp" placeholder="cp" required></td>
<td><input class="form-control form-control-sm" type="text" name="ville" id="ville" placeholder="ville" required></td>
</tr>
<tr>
<td><input onClick="AfficherMasquer()" style="margin-left:30px;" class="btn btn-primary" type="submit" value="Enregistrer"></td>
</tr>
</table>
</form>
</div>
</div>
<!-- JavaScript Bundle with Popper -->
<script type="text/javascript">
function AfficherMasquer()
{
divInfo = document.getElementById('formAjout');
 
if (divInfo.style.display == 'none')
divInfo.style.display = 'block';
else
divInfo.style.display = 'none';
 
}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>