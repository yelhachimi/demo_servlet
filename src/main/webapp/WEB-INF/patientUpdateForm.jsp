<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
<title>Modifier mon patient</title>
</head>
<body style="background-color:#F3F1F1">
<div class="container">
<table class="table table-stripped table-sm">
<tr>
<th><label for="name">Nom: </label></th>
<th><label for="prenom">Pr�nom: </label></th>
<th><label for="sexe">Sexe: </label></th>
<th><label for="dateDeNaissance">Date de naissance: </label></th>
<th><label for="numeroSecuriteSocial">Num securite social: </label></th>
</tr>

<c:forEach items="${patients}" var="patient">

<tr>
<td>
<input class="form-control form-control-sm" type="text" name="nameAffich" id="nameAffich" value="${patient.nom}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="prenomAffich" id="prenomAffich" value="${patient.prenom}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="sexeAffich" id="sexeAffich" value="${patient.sexe}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="dateDeNaissanceAffich" id="dateDeNaissanceAffich" value="${patient.dateDeNaissance}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="numeroSecuriteSocialAffich" id="numeroSecuriteSocialAffich" value="${patient.numeroSecuriteSocial}"></td>
</tr>

</c:forEach>
</table>

<h4 style="display:flex;justify-content:center;font-weight:bold;margin-bottom:10px;margin-top:50px;"><p>Modifier le patient : <span style="font-style:italic;font-weight:normal;">${patient.prenom} ${patient.nom}</span></p></h4>

<div id="formActif">
<form method="post" action="patientUpdateController">
<table class="table table-stripped">
<tr>
<td><label for="name">Nom: </label></td>
<td><label for="prenom">Pr�nom: </label></td>
<td><label for="sexe">Sexe: </label></td>
<td><label for="dateDeNaissance">Date de naissance: </label></td>
<td><label for="numeroSecuriteSocial">Num securite social: </label></td>
</tr>

<tr>
<td>
<input class="form-control form-control-sm" type="text" name="nom" id="nom" value="${patient.nom}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="prenom" id="prenom" value="${patient.prenom}"></td>
<td>
<select class="custom-select" name="sexe" id="sexe">
<option selected value="${patient.sexe}">${patient.sexe}</option>
<option value="homme">Homme</option>
<option value="femme">Femme</option>
</select>
</td>
<td>
<input class="form-control form-control-sm" type="text" name="dateDeNaissance" id="dateDeNaissance" value="${patient.dateDeNaissance}"></td>
<td>
<input class="form-control form-control-sm" type="text" name="numeroSecuriteSocial" id="numeroSecuriteSocial" value="${patient.numeroSecuriteSocial}"></td>
</tr>
</table>
<div style="display:flex;justify-content:space-around;">
<a style="margin-left:40px;" href="patient" class="btn btn-secondary"> Retour </a>
<input class="btn btn-primary" type="submit" value="Modifier">
</div>
</form>
</div>
</div>
<!-- JavaScript Bundle with Popper -->
<script type="text/javascript">
nom = document.getElementById('nom');
prenom = document.getElementById('prenom');
sexe = document.getElementById('sexe');
dateDeNaissance = document.getElementById('dateDeNaissance');
numeroSecuriteSocial = document.getElementById('numeroSecuriteSocial');

if (nom.value == "" && prenom.value == "" && sexe.value == "" && dateDeNaissance.value == "" && numeroSecuriteSocial.value == "")
	{
	nom.disabled = true;
	prenom.disabled = true;
	sexe.disabled = true;
	dateDeNaissance.disabled = true;
	numeroSecuriteSocial.disabled = true;
	}
else
	{
	nom.disabled = false;
	prenom.disabled = false;
	sexe.disabled = false;
	dateDeNaissance.disabled = false;
	numeroSecuriteSocial.disabled = false;
	}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>