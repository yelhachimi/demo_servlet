<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<form method="post" action="modifyNurse?nurse=${ nurse.id }&adress=${ adress[0] }">
			<h2>Donnees de l'infirmier.e</h2>
			<div class="row">
				<div class="mb-3 form-group col-lg-6">
					<label for="name" class="form-label">Nom: </label>
					<input type="text" class="form-control" name="name" id="name" value="${ nurse.nom }">
				</div>
				<div class="mb-3 form-group col-lg-6">
					<label for="prenom" class="form-label">Prenom: </label>
					<input type="text" class="form-control" name="prenom" id="prenom" value="${ nurse.prenom }">
				</div>
			</div>
	
			<div class="mb-3" class="form-group">
				<label for="numPro" class="form-label">Numero professionnel: </label>
				<input type="text" class="form-control" name="numPro" id="numPro" value="${ nurse.numeroProfessionnel }">
			</div>
	
			<div class="mb-3" class="form-group">
				<label for="telPro" class="form-label">Telephone professionnel: </label>
				<input type="text" class="form-control" name="telPro" id="telPro" value="${ nurse.telPro }">
			</div>
	
			<div class="mb-3" class="form-group">
				<label for="telPerso" class="form-label">Telephone personnel: </label>
				<input type="text" class="form-control" name="telPerso" id="telPerso" value="${ nurse.telPerso }">
			</div>
	
			<h2>Donnees de l'adresse</h2>
	
			<div class="row">
				<div class="mb-3 form-group col-lg-6">
					<label for="numero" class="form-label">numero: </label>
					<input type="text" class="form-control" id="numero" name="numero" value="${ adress[1] }">
				</div>
		
				<div class="mb-3 form-group col-lg-6">
					<label for="rue" class="form-label">rue: </label>
					<input type="text" class="form-control" id="rue" name="rue" value="${ adress[2] }">
				</div>
		
				<div class="mb-3 form-group col-lg-6">
					<label for="cp" class="form-label">code postal: </label>
					<input type="text" class="form-control" id="cp" name="cp" value="${ adress[3] }">
				</div>
		
				<div class="mb-3 form-group col-lg-6">
					<label for="ville" class="form-label">ville: </label>
					<input type="text" class="form-control" name="ville" id="ville" value="${ adress[4] }">
				</div>
		
				<div class="mb-3 form-group col-lg-3">
					<button class="btn btn-primary" type="submit">Modifier</button>
				</div>

				<div class="mb-3 form-group col-lg-3">
					<a href="/demo/nurse" class="btn btn-primary" type="button">Retour a la liste</a>
				</div>
			</div>
			
			<!-- <table class="table table-stripped table-sm">
				<div class="row">
					<tr>
						<div class="col-sm-1"><td><label for="name">Nom: </label></td></div>
						<div  class="col-sm-1"><td><label for="prenom">Pr�nom: </label></td></div>
						<div class="col-sm-1"><td><label for="numPro">num�ro pro: </label></td></div>
						<div class="col-sm-2"><td><label for="telPro">T�l�phone professionnel: </label></td></div>
						<div class="col-sm-3"><td><label for="telPerso">T�l�phone personnel </label></td></div>
						<div class="col-sm-1"><td></td></div>
						<div class="col-sm-1"><td></td></div>
					</tr>
				</div>
				<div>
					<tr>
						<td><label for="nameAdd">Ajouter un.e infirmier.e: </label></td>
					</tr>
				</div>
				<div class="row">
					<tr>
						<div class="col-sm-1"><td><input class="form-control form-control-sm" type="text" name="name" id="name" placeholder="Nom" value="${ nurse.nom }"></td></div>
						<div class="col-sm-1"><td><input class="form-control form-control-sm" type="text" name="prenom" id="prenom" placeholder="Prenom" value="${ nurse.prenom }"></td></div>
						<div class="col-sm-1"><td><input class="form-control form-control-sm" type="text" name="numPro" id="numPro" placeholder="Num�ro professionnel" value="${ nurse.numeroProfessionnel }"></td></div>
						<div class="col-sm-2"><td><input class="form-control form-control-sm" type="number" name="telPro" id="telPro" placeholder="t�l�phone professionnel" value="${ nurse.telPro }"></td></div>
						<div class="col-sm-3"><td><input class="form-control form-control-sm" type="number" name="telPerso" id="telPerso" placeholder="T�l�phone personnel" value="${ nurse.telPerso }"></td></div>
						
					</tr>
				</div>
				<div class="row">
					<tr>
					<div class="col-sm-2">
						<td><label for="adresse">Son adresse: </label></td>
					</div>
					</tr>
				</div>
				<div class="row">
					<tr>
						<div class="col-sm-2">
							<td><input class="form-control form-control-sm" type="text" name="numero" id="numero" placeholder="num�ro" value="${ adress[1] }"></td>
						</div>
						<div class="col-sm-2">
							<td><input class="form-control form-control-sm" type="text" name="rue" id="rue" placeholder="rue" value="${ adress[2] }"></td>
						</div>
						<div class="col-sm-2">
							<td><input class="form-control form-control-sm" type="text" name="cp" id="cp" placeholder="cp" value="${ adress[3] }"></td>
						</div>
						<div class="col-sm-2">
							<td><input class="form-control form-control-sm" type="text" name="ville" id="ville" placeholder="ville" value="${ adress[4] }"></td>
						</div>
					</tr>
				</div>
				<div class="row">
					<tr>
						<div class="col-sm-2">
							<td><input style="margin-left:30px;" class="btn btn-primary btn-sm" type="submit" value="Enregistrer"></td>
						</div>
					</tr>
				</div>
			</table> -->
		</form>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>