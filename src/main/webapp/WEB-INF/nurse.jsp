<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<body>
	<div class="container">

		<a href="/demo/index" class="btn btn-primary" type="button">Retour a la liste</a>
		<table class="table">
			<tr>
				<th scope="row"> nom </th>
				<th scope="row"> prenom </th>
				<th scope="row"> </th>
				<th scope="row"> </th>
			</tr>
			<c:forEach items="${ nurses }" var="nurse">
			<tr>
				<td> ${ nurse.nom } </td>
				<td> ${ nurse.prenom } </td>
				<td> <a href="deleteNurse?id=${ nurse.id }" class="btn btn-danger btn-sm"> Supprimer </a> </td>
				<td> <a href="modifyNurse?id=${ nurse.id }" class="btn btn-warning btn-sm"> Modifier </a> </td>
			</tr>
			</c:forEach>
		</table>
		
		<form method="post" action="nurse">
			<h2>Donnees de l'infirmier.e</h2>
			<div class="row">
				<div class="mb-3 form-group col-lg-6">
					<label for="name" class="form-label">Nom: </label>
					<input type="text" class="form-control" name="name" id="name" value="${ nurse.nom }">
				</div>
				<div class="mb-3 form-group col-lg-6">
					<label for="prenom" class="form-label">Prenom: </label>
					<input type="text" class="form-control" name="prenom" id="prenom" value="${ nurse.prenom }">
				</div>
			</div>
	
			<div class="mb-3" class="form-group">
				<label for="numPro" class="form-label">Numero professionnel: </label>
				<input type="text" class="form-control" name="numPro" id="numPro" value="${ nurse.numeroProfessionnel }">
			</div>
	
			<div class="mb-3" class="form-group">
				<label for="telPro" class="form-label">Telephone professionnel: </label>
				<input type="text" class="form-control" name="telPro" id="telPro" value="${ nurse.telPro }">
			</div>
	
			<div class="mb-3" class="form-group">
				<label for="telPerso" class="form-label">Telephone personnel: </label>
				<input type="text" class="form-control" name="telPerso" id="telPerso" value="${ nurse.telPerso }">
			</div>
	
			<h2>Donnees de l'adresse</h2>
	
			<div class="row">
				<div class="mb-3 form-group col-lg-6">
					<label for="numero" class="form-label">numero: </label>
					<input type="text" class="form-control" id="numero" name="numero" value="${ adress[1] }">
				</div>
		
				<div class="mb-3 form-group col-lg-6">
					<label for="rue" class="form-label">rue: </label>
					<input type="text" class="form-control" id="rue" name="rue" value="${ adress[2] }">
				</div>
		
				<div class="mb-3 form-group col-lg-6">
					<label for="cp" class="form-label">code postal: </label>
					<input type="text" class="form-control" id="cp" name="cp" value="${ adress[3] }">
				</div>
		
				<div class="mb-3 form-group col-lg-6">
					<label for="ville" class="form-label">ville: </label>
					<input type="text" class="form-control" name="ville" id="ville" value="${ adress[4] }">
				</div>
		
				<div class="mb-3 form-group col-lg-3">
					<button class="btn btn-primary" type="submit">Modifier</button>
				</div>
			</div>
		</form>
	</div>

	<!-- JavaScript Bundle with Popper -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>