<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


<title>Modification de d�placement</title>
</head>
<body style="background-color:#F3F1F1">

<div class= "container" style="padding:50px;">

<h3 style="display:flex;justify-content:center;margin-bottom:30px;font-weight:bold;"> Modification de D�placement</h3>

	<form method="post" action="updateDeplacementPatient">
		<div class="form-group" style="text-align:center;padding:50px;">
		<label> Identifiant du d�placement (non modifiable) :
			<input type="number" class="form-control" value="${deplacement.id }" name="id" readonly="readonly"/></label><br><br>
		<label> Identifiant du patient (non modifiable) :
			<input type="number" class="form-control" value="${deplacement.patient_id }" name="patient_id" readonly="readonly"/></label><br><br>
		<label> Identifiant de l'infirmi�re (non modifiable) :
			<input type="number" class="form-control" value="${deplacement.infirmiere_id }" name="infirmiere_id" readonly="readonly"/></label><br><br>
	
		<label> Entrez la date du d�placement :
			<input type="date" class="form-control" name="date" value="${deplacement.date }"
       min="20/08/2021" max="31/12/2099" required> 
		</label><br><br>
		
		<label> Co�t du d�placement : 
		<input type="number" class="form-control" required name="cout" min="0" value="${deplacement.cout }" step=".01">
		</label><br><br><br>
		<div class="span12" style="text-align:center">
	<a href="/demo">
      <button class="btn btn-large btn-primary">Retour � l'accueil</button>
    </a>
    		<input type="submit" value="envoyer" class="btn btn-large btn-success">
    
</div>
<br><br><br>
		</div>
	</form>
</div>


<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>