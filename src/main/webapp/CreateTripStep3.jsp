<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cr�ation de D�placement</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
</head>
<body style="background-color:#F3F1F1">

<div class= "container" style="padding:50px;">

	<h1 style="display:flex;justify-content:center;margin-bottom:30px;font-weight:bold;">Entrez les d�tails du d�placement : </h1>

	<form method="post" action="deplacement">
			<div class="form-group" style="text-align:center;padding:50px;">
	
		<label> Entrez la date du d�placement :
		
			<input type="date" class="form-control" name="date"
       min="20/08/2021" max="31/12/2099" required> 
		</label><br><br>
		
		<label> Co�t du d�placement : 
		<input type="number" class="form-control" required name="cout" min="0" value="0" step=".01">
		</label><br><br><br>
		
			<input type="number" class="form-control" name="patient_id" value=${patient_id} hidden />
			<input type="number" class="form-control" name="nurse_id" value=${nurse_id} hidden />
		<div class="span12" style="text-align:center">
	<a href="/demo">
      <button class="btn btn-large btn-primary">Retour � l'accueil</button>
      		<input type="submit" value="envoyer" class="btn btn-large btn-success"> 
    </a>
</div>
		</div>
		</form>
</div>	
<br><br><br>	
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>