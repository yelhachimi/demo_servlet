<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


<title>Le Doctolib Eco+</title>
</head>
<body style="background-color:#F3F1F1">

<div class= "container" style="padding:50px;">
<h4 style="display:flex;justify-content:center;margin-bottom:30px;font-weight:bold;">Bienvenue dans votre application de gestion d'infirmerie.</h4>

<nav class="navbar navbar-expand-lg navbar-light bg-info">
    <div class="d-md-flex d-block flex-row mx-md-auto mx-0">
    <ul class="navbar-nav">
		<li class="nav-item active"><a class="nav-link" href="/demo/patient">Afficher les patients</a></li>
		<li class="nav-item active"><a class="nav-link" href="/demo/nurse">Afficher les infirmi�res</a></li>
		<li class="nav-item active"><a class="nav-link" href="/demo/addTrip">Cr�er un d�placement</a></li>
	</ul>
	</div>
</nav>
<div class= "container" style="padding:50px;">

<span class="align-middle">Bienvenue dans votre doctoLib Eco+. Dans cette "application", vous pourrez cr�er des patients ainsi que des infirmi�res, tout en leur cr�ant des rendez-vous en renseignant la date de cette derni�re ainsi que son tarif et le patient concern� par l'intervention de l'infirmier(e).</span>
</div>

</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>