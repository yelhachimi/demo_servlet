package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.PatientEntity;
import model.PatientModel;

@WebServlet("/addTrip")
public class CreateTripStep1Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		
		try {
			List<PatientEntity> patients = con.fetchAllPatient();
			request.setAttribute("patients", patients);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("CreateTripStep1.jsp").forward(request, response);

	}
	
	

}
