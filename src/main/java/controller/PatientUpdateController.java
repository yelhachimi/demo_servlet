package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Entity.PatientEntity;
import model.PatientModel;

/**
 * Servlet implementation class PatientUpdateController
 */
@WebServlet("/patientUpdateController")
public class PatientUpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientUpdateController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		HttpSession session = request.getSession(true);
		session.setAttribute("id", request.getParameter("id"));
		
		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		
		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");

		String id = request.getParameter("id");
		
		try {
			PatientEntity patient = con.fetchPatient(id);
			request.setAttribute("patient", patient);
			List<PatientEntity> patients = con.fetchAllPatient();
			request.setAttribute("patients", patients);
			HashMap<Integer,String> nurses = con.fetchAllNurses();
			request.setAttribute("nurses", nurses);
			HashMap<Integer,String> adresses = con.fetchAllAdresses();
			request.setAttribute("adresses", adresses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("WEB-INF/patientUpdateForm.jsp").forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		
		//String id = request.getParameter("id");
		HttpSession session = request.getSession(true);
		String id = (String) session.getAttribute("id");
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String sexe = request.getParameter("sexe");
		String dateDeNaissance = request.getParameter("dateDeNaissance");
		int numeroSecuriteSocial = Integer.parseInt(request.getParameter("numeroSecuriteSocial"));
		
		try {
			con.updatePatient(nom, prenom, sexe, dateDeNaissance, numeroSecuriteSocial, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect("patientUpdateController");
	}

}
