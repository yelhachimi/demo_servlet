package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.PatientEntity;
import Entity.TripEntity;
import model.PatientModel;

/**
 * Servlet implementation class PatientController
 */
@WebServlet(name = "DeplacementPatient", urlPatterns = { "/deplacementpatient" })
public class DeplacementPatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeplacementPatientController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		
		List<TripEntity> patientDeplacements = new ArrayList<TripEntity>();

			patientDeplacements = con.getAllPatientTrips(id);
		request.setAttribute("deplacements", patientDeplacements);
		request.getRequestDispatcher("WEB-INF/PatientDeplacements.jsp").forward(request, response);
	}



}
