package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.NurseEntity;
import model.NurseModel;

/**
 * Servlet implementation class UpdateNurseController
 */
public class ModifyNurseController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyNurseController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");

		int id = Integer.parseInt(request.getParameter("id"));
		NurseEntity nurse = null;
		String [] adressDetails = null;
		
		try {
			nurse = con.getNurse(id);
			adressDetails = con.getAdress(nurse.getAdresse_id());
			System.out.println("adresse: " + adressDetails[0] + adressDetails[1] + adressDetails[2] + adressDetails[3] + adressDetails[4]);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("nurse", nurse);
		request.setAttribute("adress", adressDetails);
		
		request.getRequestDispatcher("WEB-INF/updateNurse.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Dans le doPost");
		
		int nurse_id = Integer.parseInt(request.getParameter("nurse"));
		int adress_id = Integer.parseInt(request.getParameter("adress"));
		
		NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		
		// R�cup�ration des donn�es de l'adresse
		String numero = request.getParameter("numero");
		String rue = request.getParameter("rue");
		int cp = Integer.parseInt(request.getParameter("cp"));
		String ville = request.getParameter("ville");
		
		
		try {
			con.updateAdress(adress_id, numero, rue, cp, ville);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		// R�cup�ration des donn�es de l'infirmiere
		String nom = request.getParameter("name");
		String prenom = request.getParameter("prenom");
		int numPro = Integer.parseInt(request.getParameter("numPro"));
		int telPro = Integer.parseInt(request.getParameter("telPro"));
		int telPerso = Integer.parseInt(request.getParameter("telPerso"));
		
		try {
			con.updateNurse(nurse_id, nom, prenom, numPro, telPro, telPerso);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		response.sendRedirect("nurse");
		
		/*int adresse = 0;
		
		//Donnes adresse patient
		
		System.out.println(cp);
				
		try {
			con.addAdresse(numero, rue, cp, ville);
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		//Donnes patient
		String nom = request.getParameter("name");
		String prenom = request.getParameter("prenom");
		int numPro = Integer.parseInt(request.getParameter("numPro"));
		int telPro = Integer.parseInt(request.getParameter("telPro"));
		int telPerso = Integer.parseInt(request.getParameter("telPerso"));
		try {
			HashMap<Integer,String> adresses = con.fetchAllAdresses();
			for(Entry<Integer,String> entry : adresses.entrySet())
			{
				if(entry.getValue().equals(numero+" "+rue+" "+cp+" "+ville))
				{
					adresse = entry.getKey();
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int adresse_id = adresse;
		
		try {
			con.addNurse(nom, prenom, numPro, telPro, telPerso, adresse_id);
			//con.addAdresse(numero, rue, cp, ville);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		response.sendRedirect("nurse");*/

	
	}

}
