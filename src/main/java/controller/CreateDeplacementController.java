package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.protobuf.TextFormat.ParseException;

import Entity.TripEntity;
import model.TripModel;

/**
 * Servlet implementation class User
 */
@WebServlet("/deplacement")
public class CreateDeplacementController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateDeplacementController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int patient_id = Integer.parseInt(request.getParameter("patient_id"));
		int infirmiere_id =  Integer.parseInt(request.getParameter("nurse_id"));
		Float cout =  Float.parseFloat(request.getParameter("cout"));

		TripModel con = new TripModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//TripModel con = new TripModel("jdbc:mysql://localhost:3306/medical", "root", "");


		//TripModel con = new TripModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");

		Date date = null;
		System.out.println("DATE==============="+request.getParameter("date"));
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			TripEntity deplacement = null;
			try {
				deplacement = con.createTrip(patient_id, infirmiere_id, date, cout);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		request.setAttribute("deplacement", deplacement );

		
		request.getRequestDispatcher("ConfirmationCreationDeplacement.jsp").forward(request, response);
		
		
	}

}
