package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.PatientEntity;
import Entity.TripEntity;
import model.PatientModel;

/**
 * Servlet implementation class PatientController
 */
@WebServlet(name = "UpdateDeplacementPatient", urlPatterns = { "/updateDeplacementPatient" })
public class UpdateDeplacementPatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateDeplacementPatientController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");
		
		TripEntity patientDeplacement = con.getPatientTrips(id);
		request.setAttribute("deplacement", patientDeplacement);
		request.getRequestDispatcher("WEB-INF/updatePatientDeplacement.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");	
		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		int id = Integer.parseInt(request.getParameter("id"));
		int infirmiere_id = Integer.parseInt(request.getParameter("infirmiere_id"));
		int patient_id = Integer.parseInt(request.getParameter("patient_id"));
		Date date = null;
		java.sql.Date dateDB = null;
		try {
			date = new SimpleDateFormat("yyyy-mm-dd").parse(request.getParameter("date"));
			dateDB = new java.sql.Date(date.getTime());
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Double cout = Double.parseDouble(request.getParameter("cout"));

		con.updatePatientDeplacement(id, infirmiere_id, patient_id, dateDB, cout);
		
		List<TripEntity> patientDeplacements = new ArrayList<TripEntity>();

			patientDeplacements = con.getAllPatientTrips(id);
			response.sendRedirect("/demo/deplacementpatient?id="+patient_id);
	}

}
