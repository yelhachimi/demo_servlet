package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.PatientEntity;
import Entity.TripEntity;
import model.PatientModel;

/**
 * Servlet implementation class PatientController
 */
@WebServlet(name = "DeletePatientDeplacement", urlPatterns = { "/deletePatientDeplacement" })
public class DeleteDeplacementPatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteDeplacementPatientController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		int patient_id = Integer.parseInt(request.getParameter("patient_id"));
		
		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "root");
		
		List<TripEntity> patientDeplacements = new ArrayList<TripEntity>();
			con.deletePatientTrip(id);
			patientDeplacements = con.getAllPatientTrips(patient_id);
			request.setAttribute("id", patient_id);
			request.setAttribute("deplacements", patientDeplacements);
		request.getRequestDispatcher("WEB-INF/PatientDeplacements.jsp").forward(request, response);
	}

}
