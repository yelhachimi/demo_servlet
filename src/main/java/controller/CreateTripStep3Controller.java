package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.Infirmiere;
import Entity.PatientEntity;
import model.NurseModel;
import model.PatientModel;

@WebServlet("/addTrip3")
public class CreateTripStep3Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int patient_id = Integer.parseInt(request.getParameter("patient_id"));
		int nurse_id = Integer.parseInt(request.getParameter("infirmiere_id"));
		request.setAttribute("patient_id", patient_id);
		request.setAttribute("nurse_id", nurse_id);

		request.getRequestDispatcher("CreateTripStep3.jsp").forward(request, response);

	}
	
	

}
