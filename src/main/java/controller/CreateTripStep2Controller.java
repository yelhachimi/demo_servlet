package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.Infirmiere;
import Entity.NurseEntity;
import Entity.PatientEntity;
import model.NurseModel;
import model.PatientModel;

@WebServlet("/addTrip2")
public class CreateTripStep2Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//PatientModel con = new PatientModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		
		int patient_id = Integer.parseInt(request.getParameter("id"));
		request.setAttribute("patient_id", patient_id);
		try {
			List<NurseEntity> infirmieres = con.fetchAllNurses();
			request.setAttribute("infirmieres", infirmieres);

		} catch (Exception e) {
			e.printStackTrace();
		}

		request.getRequestDispatcher("CreateTripStep2.jsp").forward(request, response);

	}
	
	

}
