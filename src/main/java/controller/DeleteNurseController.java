package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.NurseEntity;
import model.NurseModel;

/**
 * Servlet implementation class DeleteNurseController
 */
public class DeleteNurseController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteNurseController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Se connecte � la base de donn�es et supprimer l'infirmi�re de la table
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");
		NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "root");
		
		try {
			con.deleteNurse(Integer.parseInt(request.getParameter("id")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		response.sendRedirect("nurse");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
