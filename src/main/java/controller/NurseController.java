package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.NurseEntity;
import Entity.PatientEntity;
import model.NurseModel;
import model.PatientModel;

/**
 * Servlet implementation class NurseController
 */
public class NurseController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NurseController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Se connecte � la DB et recup�re toutes les infirmi�res
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");

		
		try {
			List<NurseEntity> nurses = con.fetchAllNurses();
			request.setAttribute("nurses", nurses);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("WEB-INF/nurse.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "root");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/medical", "root", "");

		//NurseModel con = new NurseModel("jdbc:mysql://localhost:3306/infirmeriejsp", "root", "Alucard_59");

		int adresse = 0;
		
		//Donnes adresse patient
		String numero = request.getParameter("numero");
		String rue = request.getParameter("rue");
		int cp = Integer.parseInt(request.getParameter("cp"));
		String ville = request.getParameter("ville");
		System.out.println(cp);
				
		try {
			con.addAdresse(numero, rue, cp, ville);
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		//Donnes patient
		String nom = request.getParameter("name");
		String prenom = request.getParameter("prenom");
		int numPro = Integer.parseInt(request.getParameter("numPro"));
		int telPro = Integer.parseInt(request.getParameter("telPro"));
		int telPerso = Integer.parseInt(request.getParameter("telPerso"));
		try {
			HashMap<Integer,String> adresses = con.fetchAllAdresses();
			for(Entry<Integer,String> entry : adresses.entrySet())
			{
				if(entry.getValue().equals(numero+" "+rue+" "+cp+" "+ville))
				{
					adresse = entry.getKey();
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int adresse_id = adresse;
		
		try {
			con.addNurse(nom, prenom, numPro, telPro, telPerso, adresse_id);
			//con.addAdresse(numero, rue, cp, ville);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		response.sendRedirect("nurse");

	}

}
