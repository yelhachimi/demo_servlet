package Entity;

import java.util.Date;

public class TripEntity {
	
	private int id;
	
	private int patient_id;
	
	private int infirmiere_id;
	
	private Date date;
	
	private Double cout;

	public TripEntity() {
	}
	
	public TripEntity(int id, int patient_id, int infirmiere_id, Date date, Double cout) {
		this.id = id;
		this.patient_id = patient_id;
		this.infirmiere_id = infirmiere_id;
		this.date = date;
		this.cout = cout;
	}

	public TripEntity(int patient_id, int infirmiere_id, Date date, Double cout) {
		this.patient_id = patient_id;
		this.infirmiere_id = infirmiere_id;
		this.date = date;
		this.cout = cout;	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(int patient_id) {
		this.patient_id = patient_id;
	}

	public int getInfirmiere_id() {
		return infirmiere_id;
	}

	public void setInfirmiere_id(int infirmiere_id) {
		this.infirmiere_id = infirmiere_id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getCout() {
		return cout;
	}

	public void setCout(Double cout) {
		this.cout = cout;
	}
	
	

}
