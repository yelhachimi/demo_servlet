package model;
import Entity.PatientEntity;
import Entity.TripEntity;

import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class TripModel extends AccessDB {

	public TripModel(String url, String username, String password) {
		super(url, username, password);
	}

	/**
	 * Function that allows the user to create a Trip
	 * @param patient_id
	 * @param infirmiere_id
	 * @param date
	 * @param cout
	 * @return the created Trip
	 * @throws Exception
	 */
	public TripEntity createTrip(int patient_id, int infirmiere_id, Date date, Float cout) throws Exception {
		
		TripEntity tripEntity = new TripEntity( patient_id, infirmiere_id, date, cout.doubleValue());
		Statement statement = this.connexion().createStatement();
		java.text.SimpleDateFormat sdf = 
			     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String currentTime = sdf.format(date);
			try {
			statement.executeUpdate("INSERT INTO deplacement (patient_id,date,cout,infirmiere_id) VALUES ('"+patient_id+"', '"+currentTime+"', '"+cout+"', '"+infirmiere_id+"');");

			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
		return tripEntity;
	}
}
