package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
//import Entity.Infirmiere;
import Entity.PatientEntity;
import java.util.List;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Entity.NurseEntity;
import Entity.PatientEntity;

public class NurseModel extends AccessDB{

	public NurseModel(String url, String username, String password) {
		super(url, username, password);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * recup�re toutes les infirmi�res
	 * @return List<NurseEntity>
	 * @throws Exception
	 */
	public List<NurseEntity> fetchAllNurses() throws Exception {
			
		List<NurseEntity> nurses = new ArrayList<NurseEntity>();
		
		Statement statement = this.connexion().createStatement();
		ResultSet result;
		
		try {
			result = statement.executeQuery("SELECT * FROM infirmiere");
			while(result.next()) {
				
				nurses.add(new NurseEntity(
						result.getInt("id"),
						result.getInt("adresse_id"),
						result.getInt("numeroProfessionnel"),
						result.getString("nom"),
						result.getString("prenom"),
						result.getInt("telPro"),
						result.getInt("telPerso")
				));			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
		return nurses;
	}
	
	/**
	 * Supprime une infirmi�re
	 * @param id => id de l'infirmi�re
	 * @throws SQLException
	 * @throws Exception
	 */
	public void deleteNurse(int id) throws SQLException, Exception {
		Statement statement = this.connexion().createStatement();
		int result;
		
		try {
			result = statement.executeUpdate("DELETE FROM infirmiere WHERE id=" + id);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * r�cup�rer toutes les adresses
	 * @return HashMap<Integer, String>
	 * @throws Exception
	 */
	public HashMap<Integer,String> fetchAllAdresses() throws Exception {
		
		HashMap<Integer,String> adresses = new HashMap<Integer,String>();
		
		Statement statement = this.connexion().createStatement();
		ResultSet result;
		
		try {
			result = statement.executeQuery("SELECT * FROM adresse");
			while(result.next()) {
				
				adresses.put(result.getInt("id"), result.getString("numero")+" "+result.getString("rue")
				+" "+result.getString("cp")+" "+result.getString("ville"));	
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
		return adresses;
	}


	/**
	 * Ajoute une adresse en BDD
	 * @param numeroAdresse
	 * @param rueAdresse
	 * @param cpAdresse
	 * @param villeAdresse
	 * @throws Exception
	 */
	public void addAdresse(String numeroAdresse, String rueAdresse, int cpAdresse, String villeAdresse) throws Exception
	{
		//Statement statement = this.connexion().createStatement();
		//ResultSet result;
		
		try {
			
			//statement.executeUpdate("INSERT INTO adresse(numero,rue,cp,ville) "+ "values("+numero+","+rue+","+cp+","+ville+")");

			//statement.executeUpdate("INSERT INTO adresse(numero,rue,cp,ville) "
					//+ "values("+numero+","+rue+","+cp+","+ville+")");
			
			//statement.execute("INSERT INTO adresse VALUES(`numeroAdresse`,`rueAdresse`,`int(cpAdresse)`,`villeAdresse`)");

			
			String query = "INSERT INTO adresse(numero,rue,cp,ville) VALUES(?,?,?,?)";
			PreparedStatement preparedStmt = this.connexion().prepareStatement(query);
		      preparedStmt.setString(1,numeroAdresse);
		      preparedStmt.setString (2, rueAdresse);
		      preparedStmt.setInt(3,cpAdresse);
		      preparedStmt.setString (4, villeAdresse);
		      preparedStmt.execute();
		      
		      
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}
	
	/**
	 * Ajoute un.e infirmier.e en BDD
	 * @param nom
	 * @param prenom
	 * @param numPros
	 * @param telPro
	 * @param telPerso
	 * @param adresse_id
	 * @throws Exception
	 */
	public void addNurse(String nom, String prenom, int numPro, int telPro, 
			int telPerso, int adresse_id) throws Exception
	{
		//Statement statement = this.connexion().createStatement();
		//ResultSet result;
		
		try {
			
			/*statement.executeUpdate("INSERT INTO patient(adresse_id,infirmiere_id,nom,prenom,dateDeNaissance,sexe,numeroSecuriteSocial) "
					+ "values("+adresse_id+","+infirmiere_id+","+nom+","+prenom+","+dateDeNaissance+","
					+sexe+","+numeroSecuriteSocial+")");*/
			
			String query = "INSERT INTO infirmiere(adresse_id, numeroProfessionnel,nom,prenom,telPro,telPerso) VALUES(?,?,?,?,?,?)";
			PreparedStatement preparedStmt = this.connexion().prepareStatement(query);
		      preparedStmt.setInt(1,adresse_id);
		      preparedStmt.setInt(2, numPro);
		      preparedStmt.setString(3,nom);
		      preparedStmt.setString(4, prenom);
		      preparedStmt.setInt(5, telPro);
		      preparedStmt.setInt(6, telPerso);
		      
		      preparedStmt.execute();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}


	/*
	 * R�cup�re un.e infirmi�re
	 */
	public NurseEntity getNurse(int id) throws SQLException, Exception {
		
		NurseEntity nurse = null;
		List<NurseEntity> nurses = this.fetchAllNurses();
		
		for(NurseEntity n: nurses) {
			if(n.getId() == id) {
				nurse = n;
			}
		}
		
		System.out.println("Nurse: " + nurse);
		
		return nurse;
	}
	
	public String[] getAdress(int id) throws SQLException, Exception {
		String[] adressDetails = new String[5];
		
		Statement statement = this.connexion().createStatement();
		ResultSet result;
		
		try {
			result = statement.executeQuery("SELECT * FROM adresse WHERE id=" + id);
			while(result.next()) {
				
				adressDetails[0] = String.valueOf(result.getInt("id"));
				adressDetails[1] = result.getString("numero");
				adressDetails[2] = result.getString("rue");
				adressDetails[3] = String.valueOf(result.getInt("cp"));
				adressDetails[4] = result.getString("ville");		
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
		return adressDetails;
	}
	
	/**
	 * Modifie une adresse existante
	 * @param id
	 * @param numero
	 * @param rue
	 * @param cp
	 * @param ville
	 * @throws SQLException
	 * @throws Exception
	 */
	public void updateAdress(int id, String numero, String rue, int cp, String ville) throws SQLException, Exception {
		Statement statement = this.connexion().createStatement();
		int result;
		
		try {
			result = statement.executeUpdate("UPDATE adresse SET numero='" + numero + "', rue='" + rue + "', cp=" + cp + ", ville='" + ville + "' WHERE id=" + id);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateNurse(int id, String nom, String prenom, int numPro, int telPro, int telPerso) throws SQLException, Exception {
		Statement statement = this.connexion().createStatement();
		int result;
		
		try {
			result = statement.executeUpdate("UPDATE infirmiere SET nom='" + nom + "', prenom='" + prenom + "', numeroProfessionnel=" + numPro
					+ ", telPro=" + telPro + ", telPerso=" + telPerso + " WHERE id=" + id);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
