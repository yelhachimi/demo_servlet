package model;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import Entity.PatientEntity;
import Entity.TripEntity;

public class PatientModel extends AccessDB {

	public PatientModel(String url, String username, String password) {
		super(url, username, password);
	}

	public List<PatientEntity> fetchAllPatient() throws Exception {

		List<PatientEntity> patients = new ArrayList<PatientEntity>();

		Statement statement = this.connexion().createStatement();
		ResultSet result;

		try {
			result = statement.executeQuery("SELECT * FROM patient");
			while (result.next()) {

				patients.add(new PatientEntity(result.getInt("id"), result.getInt("adresse_id"),
						result.getInt("infirmiere_id"), result.getString("nom"), result.getString("prenom"),
						result.getString("dateDeNaissance"), result.getString("sexe"),
						result.getInt("numeroSecuriteSocial")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}

		return patients;
	}

	public HashMap<Integer, String> fetchAllNurses() throws Exception {

		HashMap<Integer, String> nurses = new HashMap<Integer, String>();

		Statement statement = this.connexion().createStatement();
		ResultSet result;

		try {
			result = statement.executeQuery("SELECT * FROM infirmiere");
			while (result.next()) {

				nurses.put(result.getInt("id"), result.getString("nom"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}

		return nurses;
	}

	public HashMap<Integer, String> fetchAllAdresses() throws Exception {

		HashMap<Integer, String> adresses = new HashMap<Integer, String>();

		Statement statement = this.connexion().createStatement();
		ResultSet result;

		try {
			result = statement.executeQuery("SELECT * FROM adresse");
			while (result.next()) {

				adresses.put(result.getInt("id"), result.getString("numero") + " " + result.getString("rue") + " "
						+ result.getString("cp") + " " + result.getString("ville"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}

		return adresses;
	}

	public void addPatient(String nom, String prenom, String sexe, String dateDeNaissance, int numeroSecuriteSocial,
			int adresse_id, int infirmiere_id) throws Exception {
		// Statement statement = this.connexion().createStatement();
		// ResultSet result;

		try {

			/*
			 * statement.
			 * executeUpdate("INSERT INTO patient(adresse_id,infirmiere_id,nom,prenom,dateDeNaissance,sexe,numeroSecuriteSocial) "
			 * +
			 * "values("+adresse_id+","+infirmiere_id+","+nom+","+prenom+","+dateDeNaissance
			 * +"," +sexe+","+numeroSecuriteSocial+")");
			 */

			String query = "INSERT INTO patient(adresse_id,infirmiere_id,nom,prenom,dateDeNaissance,sexe,numeroSecuriteSocial) VALUES(?,?,?,?,?,?,?)";
			PreparedStatement preparedStmt = this.connexion().prepareStatement(query);
			preparedStmt.setInt(1, adresse_id);
			preparedStmt.setInt(2, infirmiere_id);
			preparedStmt.setString(3, nom);
			preparedStmt.setString(4, prenom);
			preparedStmt.setString(5, dateDeNaissance);
			preparedStmt.setString(6, sexe);
			preparedStmt.setInt(7, numeroSecuriteSocial);

			preparedStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}

	public void addAdresse(String numeroAdresse, String rueAdresse, int cpAdresse, String villeAdresse)
			throws Exception {
		// Statement statement = this.connexion().createStatement();
		// ResultSet result;

		try {

			// statement.executeUpdate("INSERT INTO adresse(numero,rue,cp,ville) "+
			// "values("+numero+","+rue+","+cp+","+ville+")");

			// statement.executeUpdate("INSERT INTO adresse(numero,rue,cp,ville) "
			// + "values("+numero+","+rue+","+cp+","+ville+")");

			// statement.execute("INSERT INTO adresse
			// VALUES(`numeroAdresse`,`rueAdresse`,`int(cpAdresse)`,`villeAdresse`)");

			String query = "INSERT INTO adresse(numero,rue,cp,ville) VALUES(?,?,?,?)";
			PreparedStatement preparedStmt = this.connexion().prepareStatement(query);
			preparedStmt.setString(1, numeroAdresse);
			preparedStmt.setString(2, rueAdresse);
			preparedStmt.setInt(3, cpAdresse);
			preparedStmt.setString(4, villeAdresse);
			preparedStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}

	public void deletePatient(String idPatient) throws Exception {
		Statement statement = this.connexion().createStatement();
		// ResultSet result;

		try {

			// statement.executeUpdate("INSERT INTO adresse(numero,rue,cp,ville) "+
			// "values("+numero+","+rue+","+cp+","+ville+")");

			// statement.executeUpdate("INSERT INTO adresse(numero,rue,cp,ville) "
			// + "values("+numero+","+rue+","+cp+","+ville+")");

			// statement.execute("INSERT INTO adresse
			// VALUES(`numeroAdresse`,`rueAdresse`,`int(cpAdresse)`,`villeAdresse`)");

			statement.executeUpdate("delete from patient where id=" + idPatient);
			/*
			 * String query = "delete from patient where id=?"; PreparedStatement
			 * preparedStmt = this.connexion().prepareStatement(query);
			 * preparedStmt.setInt(1,idPatient); preparedStmt.execute();
			 */

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}

public List<TripEntity> getAllPatientTrips(int id) {
		
		List<TripEntity> patientTrips = new ArrayList<TripEntity>();
		
		Statement statement = null;
		try {
			statement = this.connexion().createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		ResultSet result;
		
		try {
			result = statement.executeQuery("SELECT * FROM deplacement where patient_id="+id);
			while(result.next()) {
				
				patientTrips.add(new TripEntity(
						result.getInt("id"),
						result.getInt("patient_id"),
						result.getInt("infirmiere_id"),
						result.getDate("date"),
						result.getDouble("cout")
						));}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally{try {
			this.connexion().close();
		} catch (Exception e) {
			e.printStackTrace();
		}}
		return patientTrips;}

	public void updatePatient(String nom, String prenom, String sexe, String dateDeNaissance, int numeroSecuriteSocial,
			String idPatient) throws Exception {
		Statement statement = this.connexion().createStatement();

		try {

			// statement.executeUpdate("UPDATE patient set set
			// nom="+nom+",prenom="+prenom+","
			// +
			// "dateDeNaissance="+dateDeNaissance+",sexe="+sexe+",numeroSecuriteSocial="+numeroSecuriteSocial+"
			// where id="+idPatient);
			String query = "UPDATE patient set nom=?,prenom=?,dateDeNaissance=?,sexe=?,numeroSecuriteSocial=? where id=?";
			PreparedStatement preparedStmt = this.connexion().prepareStatement(query);
			preparedStmt.setString(1, nom);
			preparedStmt.setString(2, prenom);
			preparedStmt.setString(3, dateDeNaissance);
			preparedStmt.setString(4, sexe);
			preparedStmt.setInt(5, numeroSecuriteSocial);
			preparedStmt.setString(6, idPatient);

			preparedStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}

	public PatientEntity fetchPatient(String id) throws Exception {
		PatientEntity patient = null;

		Statement statement = this.connexion().createStatement();
		ResultSet result;

		try {
			result = statement.executeQuery("SELECT * FROM patient where id=" + id);
			while (result.next()) {
				patient = new PatientEntity(result.getInt("id"), result.getInt("adresse_id"),
						result.getInt("infirmiere_id"), result.getString("nom"), result.getString("prenom"),
						result.getString("dateDeNaissance"), result.getString("sexe"),
						result.getInt("numeroSecuriteSocial"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				this.connexion().close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return patient;
	}

	public void deletePatientTrip(int id) {

		Statement statement = null;
		try {
			statement = this.connexion().createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			statement.executeUpdate("Delete from deplacement where id=" + id);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				this.connexion().close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	
	public void updatePatientDeplacement(int id, int infirmiere_id, int patient_id, java.sql.Date date, Double cout) {
		Statement statement = null;
		try {
			statement = this.connexion().createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			statement.executeUpdate("update deplacement set patient_id="+patient_id+",date='"+date+"',cout="+cout+",infirmiere_id="+infirmiere_id+" where id=" + id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				this.connexion().close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public TripEntity getPatientTrips(int id) {
		TripEntity patientTrip = null;

		Statement statement = null;
		try {
			statement = this.connexion().createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		ResultSet result;

		try {
			result = statement.executeQuery("SELECT * FROM deplacement where id=" + id);
			while (result.next()) {

				patientTrip = new TripEntity(result.getInt("id"), result.getInt("patient_id"),
						result.getInt("infirmiere_id"), result.getDate("date"), result.getDouble("cout"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				this.connexion().close();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return patientTrip;
	}
}
